#!/bin/bash
set -x
myroute=$(echo $ifconfig_local | python3 -c 'from ipaddress import IPv4Network; import os, sys; sys.stdout.write("{}\n".format(IPv4Network("{}/32".format(os.getenv("ifconfig_local"))).supernet(new_prefix=24)))' | tr -d '\n')
id=$(echo "1$(echo $dev | tr -d '[a-z]' | tr -d '[A-Z]' | tr -d '\n')")

#echo "myroute " $myroute

# stand interface up
ip link set up dev $dev

# reset rules (private route table / lookup rule)
ip rule flush table $id

# reset interface
ip addr flush dev $dev
ip -6 addr flush dev $dev

#reset routes (private route table)
ip route flush table $id
ip -6 route flush table $id

# add fwmark and lookup rule
ip rule add fwmark "${id}" table $id
ip rule add to "${ifconfig_local}" lookup $id

# add addresses without prefix (add prefix routes second and specify private route table to add to)
ip addr add "${ifconfig_local}/24" dev $dev
ip -6 addr add fe80:$id::ffff/64 dev $dev

# delete auto added routes from addr add
ip route flush dev $dev
ip -6 route flush dev $dev

# add prefix routes (in private table)
ip route add $myroute dev $dev src $ifconfig_local table $id
ip -6 route add fe80::/10 src fe80:$id::ffff dev $dev table $id

# add default gateway (on private route table)
ip route add default via $route_vpn_gateway src $ifconfig_local dev $dev table $id
